#include <iostream>
#include <string.h>
using namespace std;




int main() {
    
    // Correctoin by Dr. Green
    // This works (no points lost), but should generally be a std::string type, not char
    char stringOne[100];
    char stringTwo[100];
    int hCount;

    //Tells the user what the program will do.
    cout << "This program will calcuate the hamming distance between two strings inputed by the user." << endl; 


    // Correction by Dr. Green
    // The program is supposed ot loop until a sentinel value is entered. In other words,
    // the program should run 'while' the sentinel value has not been entered
    // You would basically wrap most of your program inside a while loop
    // that checks such conditions.

    //User Input for both strings
    cout << "Enter the first string of characters:" << endl; 
    cin >> stringOne;
    cout << "Enter the second string of characters:" << endl;
    cin >> stringTwo;



    //Check string length to be greater than 0
    // Correction by Dr. Green
    // This was formulated improperly. Each clause must 
    // have the full logic.
    if (strlen(stringOne) ==0 || strlen(stringTwo) == 0){
    // if (strlen(stringOne) || strlen(stringTwo) == 0){
        cout << "The length of each string must be at least one. You entered an empty or invalid string." << endl;
        system("pause");
        exit(0);
    }

    //Check string length
    if (strlen(stringOne) != strlen(stringTwo)){
        cout << "The two strings length do not match. They must be the same length." << endl;
        system("pause");
        exit(0);
    } else {

        // Correctoin by Dr. Green
        // You need to initialize hCount. Unitialized variables contain garbage.
        // So in this case, we initialize to 0
        hCount = 0;
        //Calclaute Hamming Distance
        for (int i=0; stringOne[i]; i++){
            if (stringOne[i] != stringTwo[i]){
                hCount++;
            }
        }
        cout << "The Hamming Distance is: " << hCount << endl;
    }
        // system("pause");
        return 0;
}

